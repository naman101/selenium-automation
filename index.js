require("chromedriver");
const fs = require('fs');
const webdriver = require('selenium-webdriver'),  //Initialise webDriver
    By = webdriver.By,
    until = webdriver.until,
    actions = webdriver.actions;

var configFile = JSON.parse(fs.readFileSync('config.json','utf-8'));  //reads the config data
var parse = require('./parseString');
var driver = new webdriver.Builder() //Initialises firefox driver
    .forBrowser('firefox')
    .build();



driver.get('https://test.salesforce.com/?un=lmccrary@pcyc.com.apttus.gk&pw=Javascript2018');  //the webdriver opens the salesforce main homepage via the login link
// driver.findElement(By.id('username')).sendKeys(configFile.credentials.username).then(() => {    //Use this incase of username and password
//         console.log('Username entered');
//     });
//
// driver.findElement(By.id('password')).sendKeys(configFile.credentials.password).then(() => {    //Sends the Password from the config file
//         console.log("Password Entered");
//     });
// driver.findElement(By.id('Login')).click().then(() => {    //Clicks on the login Button
//         console.log("Login Clicked");
//     });

driver.wait(until.elementLocated(By.linkText('New Agreement Request'))).click().then(() => {
  console.log("Agreement Started");
});
driver.wait(until.elementLocated(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:vendor_select_aco'))).sendKeys(configFile.vendor);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:0:name')).click().then(() => {
  console.log('Vendor/Party Entered');
});

driver.wait(until.elementIsVisible(driver.findElement(By.id('loading-spinner'))));
driver.wait(until.elementIsNotVisible(driver.findElement(By.id('loading-spinner'))));


driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:0:name')).sendKeys(configFile.businessContactName);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:0:title')).sendKeys(configFile.businessContactTitle);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:0:email')).sendKeys(configFile.businessContactEmail);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:0:phone')).sendKeys(configFile.businessContactPhone).then(() => {
  console.log("Business Contact Details Entered");
});
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:1:name')).sendKeys(configFile.legalContactName);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:1:title')).sendKeys(configFile.legalContactTitle);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:1:email')).sendKeys(configFile.legalContactEmail);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:vendor_main_section:j_id40:vendor_component:j_id43:0:j_id62:1:phone')).sendKeys(configFile.legalContactPhone).then(() => {
  console.log("Legal Contact Details Entered");
});
driver.findElement(By.id('procurement_type_ahead_0')).click();
driver.wait(until.elementLocated(By.xpath(parse('//P[@class=\'suggestion-procurement_type_ahead_0 tt-suggestion tt-selectable\'][text()=\'%s\']', configFile.purchaseCategory)))).click();
driver.findElement(By.tagName('html')).click().then(() => {
  console.log('Purchase Category Entered');
});

driver.wait(until.stalenessOf(driver.findElement(By.css('.loading-block'))));

driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:j_id123:0:purchasing_bli_input')).sendKeys(configFile.budgetLineItem);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:j_id123:0:purchasing_esb_input')).sendKeys(configFile.estimatedBreakdown);
driver.findElement(By.tagName('html')).click();

driver.wait(until.stalenessOf(driver.findElement(By.css('.loading-block')))); //Waits for ajax calls.
driver.sleep(5000); //Waits for ajax calls

driver.wait(until.elementIsVisible(driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:comment_box'))));
driver.wait(until.elementIsEnabled(driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:comment_box')))).click();
driver.wait(until.elementLocated(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:comment_box'))).sendKeys(configFile.purchaseCategoryComments);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:pc_vendors:vendor_component:j_id254:0:j_id273:3:name')).sendKeys(configFile.payeeContactName);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:pc_vendors:vendor_component:j_id254:0:j_id273:3:title')).sendKeys(configFile.payeeContactTitle);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:pc_vendors:vendor_component:j_id254:0:j_id273:3:email')).sendKeys(configFile.payeeContactEmail);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id116:j_id117:purchase_category_section:j_id118:j_id119:pc_vendors:vendor_component:j_id254:0:j_id273:3:phone')).sendKeys(configFile.payeeContactPhone).then(() => {
  console.log("Payee Contact Details Entered.")
});
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:project_lead_section_item:project_lead_input')).sendKeys(configFile.projectLeadName);
driver.findElement(By.tagName('html')).click();


driver.wait(until.elementIsVisible(driver.findElement(By.id('loading-spinner'))));
driver.wait(until.elementIsNotVisible(driver.findElement(By.id('loading-spinner'))));

driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:business_owner_section_item:business_owner_input')).sendKeys(configFile.budgetOwnerName);
driver.findElement(By.tagName('html')).click();

driver.wait(until.elementIsVisible(driver.findElement(By.id('loading-spinner'))));
driver.wait(until.elementIsNotVisible(driver.findElement(By.id('loading-spinner'))));

driver.wait(until.elementLocated(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:project_code_section_item:project_code'))).click();
driver.findElement(By.xpath(parse('//*[@id="j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:project_code_section_item:project_code"]/option[@value = "%s"]',configFile.projectCode))).click().then(() => {
  console.log("Project Lead, Budget Owner,  and Project Code Entered.")
});
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:service_start_date_section_item:serviceStartDate')).sendKeys(configFile.startDate);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:agreement_detail_component:j_id39:j_id333:j_id334:project_details:service_end_date_section_item:serviceEndDate')).sendKeys(configFile.endDate);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:request_type:agmt_type_si:agreement_type')).click();
driver.findElement(By.xpath(parse('//*[@id="j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:request_type:agmt_type_si:agreement_type"]/option[@value = "%s"]', configFile.agreementType))).click().then(() => {
  console.log("Agreement Type Selected");
});

driver.wait(until.stalenessOf(driver.findElement(By.id('loading-spinner'))));

driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:ExisingAbbvieAgrSectionItem:PCYC_existing_AbbVie_agreement_present')).click();

driver.wait(until.elementLocated(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:ExisingAbbvieAgrSectionItem:PCYC_existing_AbbVie_agreement_present'))).click();;
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:ExisingAbbvieAgrSectionItem:PCYC_existing_AbbVie_agreement_present')).sendKeys(configFile.abbvie);

driver.wait(until.stalenessOf(driver.findElement(By.id('loading-spinner'))));

driver.wait(until.elementLocated(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:AbbvieContactDetailSectionItem:PCYC_AbbVie_contact_name_and_e_mail'))).sendKeys(configFile.abbvieData);
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:ServicesDetailSectionItem:PCYC_Describe_Services_Being_Requested')).sendKeys(configFile.services);

driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:GxPSectionItem:PCYC_Is_GxP_Compliant')).click();
driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:j_id854:j_id855:agmt_ms:MS_Component:MasterServices_AgreementSection:GxPSectionItem:PCYC_Is_GxP_Compliant')).sendKeys(configFile.gxp);


driver.findElement(By.id('j_id0:request_wizard:main_page_block:PCYC_Request_Wizard_Inside_Component:j_id38:vendor_main_section:signatory_vendors:vendor_component:j_id1868:0:same_as_bc')).click();
driver.wait(until.stalenessOf(driver.findElement(By.id('loading-spinner'))));
driver.findElement(By.id('j_id0:request_wizard:main_page_block:pgButtons:bottom:SubmitBtn')).click().then(() => {
  console.log('Agreement Submitted.');
});

driver.sleep(5000);
